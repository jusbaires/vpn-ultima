<?php namespace Config;

/**
 * Holds the paths that are used by the system to
 * locate the main directories, app, system, etc.
 * Modifying these allows you to re-structure your application,
 * share a system folder between multiple applications, and more.
 *
 * All paths are relative to the project's root folder.
 */

class Paths
{
	public $systemDirectory = __DIR__ . '/../ThirdParty/codeigniter4/framework/system';

	public $appDirectory = __DIR__ . '/..';

	public $writableDirectory = __DIR__ . '/../Writable';

	public $testsDirectory = __DIR__ . '/../tests';

	public $viewDirectory = __DIR__ . 'Views';
}
