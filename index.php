<?php

define('BASEURL',  (isset($_SERVER['HTTP_X_FORWARDED_PROTO'])?  strtok($_SERVER['HTTP_X_FORWARDED_PROTO'], ','):'http') .'://' . $_SERVER['HTTP_HOST']. '/'. substr(pathinfo($_SERVER['SCRIPT_NAME'], PATHINFO_DIRNAME), 1) . '/' );

// Fix rapido a falta de intl
if(!function_exists('locale_set_default')) {
    function locale_set_default($a) {
    }
}

// Defino la ruta base del sitio
define('ROOTPATH', __DIR__.'/');

// Marco el sitio como entorno de desarollo si estas en modo servidor integrado
if(php_sapi_name() == 'cli-server') {
    define('ENVIRONMENT', 'development');
   
    if(!file_exists('Components/package-lock.json') or filemtime('Components/package-lock.json') < filemtime('Components/package.json') ) {
        shell_exec('npm install --prefix Components');
    }
    
    if(!file_exists('composer.lock') or filemtime('composer.lock') < filemtime('composer.json') ) {
        shell_exec('composer install');
    }
    
    
    function getDirContents($dir, &$results = array()) {
        $files = scandir($dir);
    
        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            } else if ($value != "." && $value != "..") {
                getDirContents($path, $results);
                $results[] = $path;
            }
        }
    
        return $results;
    }
    
    
    
    
    $todos_los_archivos = getDirContents('Components');
    
    
    
    
    $todos_los_archivos = array_filter($todos_los_archivos, function($ruta) {
        $exclude = __DIR__. '/Components/node_modules/';
        return substr($ruta, 0, strlen($exclude)) != $exclude;
    });
    
    $ultima_modificacion_modulos = max(array_map('filemtime', $todos_los_archivos));
    

    if(!file_exists('Scripts/components.js') or $ultima_modificacion_modulos > filemtime('Scripts/components.js')) {
        chdir('Components');
        
        $salida_comando = array();
        $estado_comando = "";
        
        exec('webpack build', $salida_comando, $estado_comando);
        
        if($estado_comando)  {
            echo "<h1>ERROR DE COMPILACION DE REACT</h1>";
            echo "<pre>";
            
            foreach($salida_comando as $linea_del_comando) {
                echo htmlspecialchars("$linea_del_comando\n");
            }
            
            echo "</pre>";
            die;
        }

        chdir('..');
    }
} else {
    define('ENVIRONMENT', 'production');
}

require 'ThirdParty/autoload.php';

// Valid PHP Version?
$minPHPVersion = '7.2';
if (phpversion() < $minPHPVersion)
{
	die("Your PHP version must be {$minPHPVersion} or higher to run CodeIgniter. Current version: " . phpversion());
}
unset($minPHPVersion);

// Path to the front controller (this file)
define('FCPATH', __DIR__ . DIRECTORY_SEPARATOR);

// Location of the Paths config file.
// This is the line that might need to be changed, depending on your folder structure.
$pathsPath = realpath(FCPATH . 'Config/Paths.php');
// ^^^ Change this if you move your application folder

/*
 *---------------------------------------------------------------
 * BOOTSTRAP THE APPLICATION
 *---------------------------------------------------------------
 * This process sets up the path constants, loads and registers
 * our autoloader, along with Composer's, loads our constants
 * and fires up an environment-specific bootstrapping.
 */

// Ensure the current directory is pointing to the front controller's directory
chdir(__DIR__);

// Load our paths config file
require $pathsPath;
$paths = new Config\Paths();

// Location of the framework bootstrap file.
$app = require rtrim($paths->systemDirectory, '/ ') . '/bootstrap.php';

/*
 *---------------------------------------------------------------
 * LAUNCH THE APPLICATION
 *---------------------------------------------------------------
 * Now that everything is setup, it's time to actually fire
 * up the engines and make this app do its thang.
 */
$app->run();
