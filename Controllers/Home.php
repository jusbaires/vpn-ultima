<?php

namespace App\Controllers;

use \Config\Services;

class Home extends BaseController
{
    public function __construct()
    {
        $this->request = Services::request();
    }     	

public function index()
	{
        $this->require_valid_user();        
	    return view('home');
	}
}