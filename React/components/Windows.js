import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';
import Collapse, {Panel} from 'rc-collapse';
import '../node_modules/rc-collapse/assets/index.css';
import WindowsSinCert from "./WindowsSinCert.js";
import WindowsConCert from "./WindowsConCert.js";


class Windows extends Component {
  render() {
    return (
        <div>
            <div className="subsection">
                <div className="comment">
                    Si a Ud. se le ha otorgado un certificado digital para acceso remoto <b>(solo proveedores y personal interno determinado)</b> seleccione
                    la opción <b>"Configuración VPN Con Certificado"</b>. Caso contrario, seleccione la opción <b>"Configuración VPN Sin Certificado"</b>.
                </div>
                <br/>
                <br/>
                <div className="alert alert-danger" role="alert">
                  <span><b>NOTA:</b> Siguiendo todos los pasos enumerados a continuación podrá conectarse a la red de manera remota. </span>
                </div>
            </div>
            <br/>
            <Collapse>
              <Panel header="Configuración VPN Sin Certificado"><WindowsSinCert/></Panel>
              <Panel header="Configuración VPN Con Certificado"><WindowsConCert/></Panel>
            </Collapse>
            <br/>
        </div>
        );
    }
}

export default Windows;