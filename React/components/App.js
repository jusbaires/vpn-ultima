import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Header from './Header.js';
import Footer from './Footer.js';
import SectionA from './SectionA.js';
import SectionB from './SectionB.js';
import SectionC from './SectionC.js';
import SectionD from './SectionD.js';
import SectionE from './SectionE.js';
import SectionF from './SectionF.js';
import FAQ from './FAQ.js';
import '../styles/Styles.css';
import img from '../images/Consejo.png';




export default class App extends Component {
  
  render() {
    
    return (
        <div>
            <Header/>
            <img src={img} alt="consejo" className="imageTop"></img>
            <SectionA/>
            <SectionB/>
            <SectionC/>
            <SectionD/>
            <SectionE/>
            <SectionF/>
            <FAQ/>
            <Footer/>
        </div>
    )
  }
        
}

ReactDOM.render(<App/>, document.getElementById('App'));
