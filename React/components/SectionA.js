import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';



class SectionA extends Component {
  
  constructor(props){
        super(props);
        this.state = {
            title: this.props.title,
            content: this.props.content
        }
    }
    
    
  
  render() {
      
    return (
        
         <div className="whiteBg">

                <div id="sectionA" className="sectionA top_layer">
                 <div className="container">
                    <div className="row">
                       <div className="col-md-12">
                            <div className="titlepage">
                                 <h2>¿Qué es una VPN?</h2>
                                <span>El término VPN <b>(Virtual Private Network)</b> o Red privada Virtual,  es una tecnología de red que permite conectar una o más computadoras en una red privada virtual, a través de una red pública como Internet, sin necesidad que las computadoras estén conectadas físicamente entre sí o de que estén en un mismo lugar. <br/></span>
                                <br/>
                                <span><b>     Esto significa que configurando la siguiente VPN que detallamos a continuación en su computadora personal, podrá acceder a la red del CMCABA desde cualquier lugar con tal solo una conexión de internet.</b></span>
                            </div>
                       </div>
                    </div>
                 </div>
                 </div>

         </div>
  
    )
  }
  
  
}


export default SectionA;