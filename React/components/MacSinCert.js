import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';
import image1 from '../images/MacSinCert/image1.png';
import image2 from '../images/MacSinCert/image2.jpg';
import image3 from '../images/MacSinCert/image3.jpg';
import image4 from '../images/MacSinCert/image4.jpg';
import image5 from '../images/MacSinCert/image5.jpg';
import image6 from '../images/MacSinCert/image6.jpg';
import image7 from '../images/MacSinCert/image7.jpg';
import image8 from '../images/MacSinCert/image8.jpg';
import image9 from '../images/MacSinCert/image9.jpg';
import image11 from '../images/MacSinCert/image11.jpg';
import image12 from '../images/MacSinCert/image12.png';
import image13 from '../images/MacSinCert/image13.jpg';
import image14 from '../images/MacSinCert/image14.jpg';
import MacEscritorioRemoto from './MacEscritorioRemoto';



class MacSinCert extends Component {
  render() {
    return (
        <div>
          <div className="col-md-12"></div>
            <div className="col-md-12">
                <div id="subsectionMacSinCert" className="subsection">
                 <div className="container">
                    <div className="row">
                       <div className="col-md-12">
                            <div className="titlepage">
                                <h2>Configuración de su computadora personal para acceso remoto con Sistema Operativo MacOS - Sin Certificado</h2>
                                <span>A continuación se describen los pasos necesarios a fin de realizar la descarga e instalación de aplicativos para la conexión remota al CMCABA.</span>
                                <br/>
                                <ol start="1">
                                    <li>
                                        <span><b>Descargar e Instalar Software Cisco AnyConnect</b></span>
                                    <ol>
                                        <li>Este procedimiento lo realizará <b>sólo una vez</b>, para la descarga e instalación del aplicativo <a href="/React/public/downloads/MacOS/anyconnect-macos-4.9.02028-predeploy-k9.dmg" download>Cisco AnyConnect MacOS (click aquí)</a></li>
                                        <li>Al finalizar descarga, hacer doble click sobre el ícono respectivo</li><img src={image1}/><img src ={image2}/>
                                        <li>Se despliega el asistente de instalación. Hacer click en <b>Continuar</b>.</li><img src={image3}/>
                                        <li> En la siguiente pantalla se muestra el contrato de licencia, hacer click sobre el botón <b>Continuar</b>.</li><img src={image4}/>
                                        <li> Hacer click sobre el botón <b>Continuar</b>.</li><img src={image5}/>
                                        <li> En la siguiente imagen los valores predeterminados, deben permanecer tal como se muestran. Hacer click sobre el botón <b>Acepto</b>.</li><img src={image6}/>
                                        <li> En la siguiente pantalla, tildar los items señalados y hacer click sobre el botón <b>Continuar</b>.</li><img src={image7}/>
                                        <li> En el siguiente cuadro de diálogo, ingresar la contraseña de administrador del equipo. Luego hacer click sobre el botón <b>Instalar Software</b>.</li><img src={image8}/>
                                        <li> Se muestra a continuación la imagen que confirma que ha finalizado el proceso de instalación de manera exitosa.</li><img src={image9}/>
                                    </ol>
                                    </li>
                                <br/>
                                    <li>
                                        <span><b>Conexión a la VPN</b></span>
                                    <ol>
                                        <li>Ubique ícono del AnyConnect en la Barra de Menú o abra la carpeta aplicaciones, y dentro de la carpeta Cisco encontrará la aplicación “Cisco AnyConnect Secure Mobility Client.app”,
                                        luego abra la aplicación con un doble click.</li><img src={image11}/>
                                        <li>Al abrir la aplicación escribir  <b>“vp.jusbaires.gob.ar”</b> y click en <b>Connect</b>.</li><img src={image12}/>
                                        <li>En Group seleccionar <b>“SinCertificado”</b>. Introduzcir su usuario y contraseña (que utiliza para el acceso al CMCABA). Y click en <b>Ok</b> para conectar.</li><img src={image13}/>
                                        <li>Seguidamente cuando se conecte a la VPN del CMCABA se mostrará esta información indicando que está correctamente conectado.</li><img src={image14}/>
                                    </ol>
                                    </li>
                                </ol>
                            </div>
                            <MacEscritorioRemoto setClass={'startThree'}/>
                       </div>
                    </div>
                 </div>
                 </div>
             </div>
             <div className="col-md-12"></div>
        </div>
        )
    }
}

export default MacSinCert;
