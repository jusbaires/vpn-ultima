import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';
import Collapse, {Panel} from 'rc-collapse';
import '../node_modules/rc-collapse/assets/index.css';
import MacOS from './MacOS.js';
import Windows from './Windows.js';
import Linux from './Linux.js';
import VDIExterior from './VDIExterior.js';

class Collapsible extends Component {
  render() {
    return (
        <div>
            <br/>
            <Collapse>
              <Panel header="Acceso remoto desde Windows" id="saltoSectionF"><Windows/></Panel>
              <Panel header="Acceso remoto desde MacOS"><MacOS/></Panel>
              <Panel header="Acceso remoto desde Ubuntu"><Linux/></Panel>
              <Panel header="Acceso a VDI desde el Exterior"><VDIExterior/></Panel>
            </Collapse>
        </div>
        )
    }
}

export default Collapsible;