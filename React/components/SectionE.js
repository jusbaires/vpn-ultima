import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';
import Collapsible from './Collapsible.js';


class SectionE extends Component {

  render() {
      
    return (
         <div className="whiteBg">
            <div className="col-md-12"></div>
            <div className="col-md-12">
                <div id="sectionD" className="section">
                 <div className="container">
                    <div className="row">
                       <div className="col-md-12">
                            <div className="titlepage">
                                <h2>Ya tengo mi acceso a la VPN, ¿ahora que debo hacer?</h2>
                                <span>Una vez que la mesa de ayuda le informe que tiene el acceso a la VPN deberá realizar la configuración de la misma. A continuación elija el sistema operativo de su computadora a fin de obtener el paso a paso:</span>
                                <br/>
                                <Collapsible/>
                            
                            </div>
                       </div>
                    </div>
                 </div>
                 </div>
             </div>
             <div className="col-md-12"></div>
         </div>
    )
  }
  
}

export default SectionE;