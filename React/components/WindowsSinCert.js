import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';
import imgWin from '../images/WindowsSinCert/EjecutarWindows.png';
import imgNext1 from '../images/WindowsSinCert/next1.png';
import imgNext2 from '../images/WindowsSinCert/next2.png';
import imgInstall from '../images/WindowsSinCert/install.png';
import imgFinish from '../images/WindowsSinCert/finish.png';
import imgTS from '../images/WindowsSinCert/TS-CMCABA.png';
import imgTSyANY from '../images/WindowsSinCert/TSyANY.png';
import ejecutarCisco from '../images/WindowsSinCert/EjecutarCisco.png';
import connect from '../images/WindowsSinCert/connect.png';
import pressOK from '../images/WindowsSinCert/pressOK.png';
import accesoRemoto from '../images/WindowsSinCert/accesoRemoto.png';
import credenciales from '../images/WindowsSinCert/credenciales.png';
import WinEscritorioRemoto from "./WinEscritorioRemoto.js";


class WindowsSinCert extends Component {
    render(){
    return(
        <div>
          <div className="col-md-12"></div>
            <div className="col-md-12">
                <div id="subsectionMacSinCert" className="subsection">
                 <div className="container">
                    <div className="row">
                       <div className="col-md-12">
                            <div className="titlepage">
                                <h2>Configuración de su computadora personal para acceso remoto con Sistema Operativo Windows - Sin Certificado</h2>
                                <span>A continuación se describen los pasos necesarios a fin de realizar la descarga e instalación de aplicativos para la conexión remota al CMCABA.</span>
                                <br/>
                                <ol start="1">
                                    <li>
                                        <span><u><b>Descargar e Instalar Software Cisco AnyConnect</b></u></span>

                                    <p>Este procedimiento lo realizará <b>sólo una vez</b>, para la descarga e instalación del aplicativo <a href="/React/public/downloads/Windows/anyconnect-win-4.8.03052-core-vpn-predeploy-k9.msi">Cisco AnyConnect (click aquí)</a></p>
                                        <ol>
                                            <li>Ejecutar archivo</li><img src={imgWin} alt="capturaEjecucion"/>
                                            <li>Next en la siguiente pantalla:</li><img src={imgNext1} alt="capturaNext"/>
                                            <li>A continuación aparecerá  la licencia de usuario, la cual debe aceptar “I accept the terms in the Licence Agreement” y presionar Next:</li>
                                            <img src={imgNext2} alt="capturaNext"/>
                                            <br/>
                                            <li>A continuación, presione  Install:</li>
                                            <img src={imgInstall} alt="capturaInstall"/>
                                            <br/>
                                            <li>Una vez finalizada la instalación, presione Finish en la siguiente pantalla (tardará unos minutos):</li>
                                            <img src={imgFinish} alt="capturaFinish"/>
                                            <br/>
                                        </ol>
                                    </li>
                                    <br/>
                                    <li>
                                        <span><u><b>Establecer la Conexión</b></u></span>

                                    <br/>
                                    <span>Este procedimiento deberá ejecutarlo cada vez que requiera conectarse a la VPN y al escritorio Remoto. Para ello AMBOS ÌCONOS DEBEN ESTAR EN EL ESCRITORIO DE LA PC</span>
                                        <ol>
                                            <li>Primero debe conectarse a la VPN</li><img src={imgTSyANY} alt="imgTSyANY"/>
                                            <br/>
                                                <div className="alert alert-danger" role="alert">
                                                    <span>En Windows 8, 8.1 o 10, si no aparece el ícono <b><i>“Cisco AnyConnect Secure”</i></b> en el escritorio, debe buscarlo utilizando las palabras claves <b><i>vpn</i></b> o <b><i>cisco</i></b></span>
                                                </div>
                                            <li>Ejecutar el “Cisco AnyConnect Secure Mobility Client”</li>
                                            <img src={ejecutarCisco} alt="ejecutarCisco"/>
                                            <br/>
                                            <li>Luego escribir: vp.jusbaires.gob.ar;  y presionar Connect</li>
                                            <img src={connect} alt="connect"/>
                                            <br/>
                                            <li>En el siguiente paso será solicitado el grupo y el password de acceso de red. Dónde:</li>
                                                <ol type="a" className="tabbed">
                                                    <li>En Group: Debe seleccionar de la lista: SinCertificado.</li>
                                                    <li>Username: Introduzca el Usuario de red asignado por el CMCABA.</li>
                                                    <li>Password: Introduzca la Contraseña de red asignada por el CMCABA.</li>
                                                    <img src={pressOK} alt="imgOK" />
                                                    <br/>
                                                    <li>Finalmente presione OK.</li>
                                                    <li>En caso de aparecer una advertencia de seguridad: marcar "No volver a preguntar..." y presionar SI</li>
                                                </ol>
                                        </ol>
                                    </li>
                                </ol>
                                </div>
                                <WinEscritorioRemoto setClass={'startThree'}/>


                       </div>
                    </div>
                 </div>
                 </div>
             </div>
             <div className="col-md-12"></div>
        
            
            
        </div>
        
        );
    }
}

export default WindowsSinCert;
