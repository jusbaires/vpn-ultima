import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';
import accesoRemoto from '../images/WindowsSinCert/accesoRemoto.png';
import credenciales from '../images/WindowsSinCert/credenciales.png';
import escritorioRemoto from '../images/TS.JPG'

class WinEscritorioRemoto extends Component{
    render(){
        return(
        <div>
             <div className="subsection">
             <div className="container">
                    <div className="row">
                       <div className="col-md-12">
                            <div className="titlepage">
                            <ol id={this.props.setClass}>
                                <li id={this.props.display}>
                                    <span><b>Descargar e Instalar el Acceso Remoto</b></span>
                                    <br/>
                                    <ol>
                                        <li>Descargar el Acceso al Escritorio Remoto <a href="/React/public/downloads/TS-CMCABA.rdp" download>TS-CMCABA (click aquí)</a></li>
                                        <li>Una vez descargado debe Guardar Archivo:</li>
                                        <li>Buscar  en la carpeta de descargas y copiar al escritorio de su computador el  TS-CMCABA.</li>
                                    </ol>
                                    <br/>
                                </li>
                                <li>
                                    <span><b>Conectarse al Escritorio Remoto</b></span>
                                    <ol>
                                        <li>Buscar el ícono TS-CMCABA previamente copiado en el escritorio.</li>
                                        <li>Doble click en el ícono TS-CMCABA.</li>
                                        <li>Seleccionar No volver a Preguntar y Presionar Conectar.</li><img src={accesoRemoto} alt="imgRemoto"/>
                                        <br/>
                                        <li>Escribir Credenciales (Usuario y Contraseña otorgados por el CMCABA), seleccionar Recordar mis Credenciales y Aceptar:</li><img src={credenciales} alt="credenciales"/>
                                        <br/>
                                        <span><b>En este momento tendrá acceso al escritorio Remoto.</b></span> <img src ={escritorioRemoto}/>

                                    </ol>
                                </li>
                            </ol>
                               
                            </div>  
                        </div>  
                    </div>   
                </div>  
            </div>  
        
        
                
        </div>
    );   
    }
}

export default WinEscritorioRemoto;
