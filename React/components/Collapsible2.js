import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';
import Collapse, {Panel} from 'rc-collapse';
import '../node_modules/rc-collapse/assets/index.css';
import RenCert from './RenCert.js';

class Collapsible2 extends Component {
  render() {
    return (
        <div>
            <br/>
            <Collapse>
              <Panel header="Reinstalación de certificado VPN"><RenCert/></Panel>
            </Collapse>
        </div>
        )
    }
}

export default Collapsible2;