import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';
import RC1 from '../images/RenCert/imagen1.png';
import RC2 from '../images/RenCert/imagen2.png';
import RC3 from '../images/RenCert/imagen3.png';
import RC4 from '../images/RenCert/imagen4.png';
import RC5 from '../images/RenCert/imagen5.png';
import RC6 from '../images/RenCert/imagen6.png';
import RC7 from '../images/RenCert/imagen7.png';
import RC8 from '../images/RenCert/imagen8.png';
import RC9 from '../images/RenCert/imagen9.png';
import RC10 from '../images/RenCert/imagen10.png';
import RC11 from '../images/RenCert/imagen11.png';
import RC12 from '../images/RenCert/imagen12.png';
import RC13 from '../images/RenCert/imagen13.png';
import RC14 from '../images/RenCert/imagen14.png';
import RC15 from '../images/RenCert/imagen15.png';

class RenCert extends Component {
    render(){
    return(
        <div>
          <div className="col-md-12"></div>
            <div className="col-md-12">
                <div id="subsectionRenCert" className="subsection">
                 <div className="container">
                    <div className="row">
                       <div className="col-md-12">
                            <div className="titlepage">
                                <h2>Eliminación del Certificado Actual:</h2>
                                <ol>
                                    <li>Ir a Inicio de Windows y buscar el programa mmc.exe, luego presione Enter.</li>
                                    <img src={RC1} alt="fotoRC1"/>
                                    <li>Luego, Archivo, Agregar o quitar complemento….</li>
                                    <img src={RC2} alt="fotoRC2"/>
                                    <li>Seleccionar Certificados y luego presionar <b>Agregar</b>.</li>
                                    <img src={RC3} alt="fotoRC3"/>
                                    <li> Se desplegará una ventana. seleccionar Mi cuenta de usuario y presionar <b>Finalizar</b>.</li>
                                    <img src={RC4} alt="fotoRC4"/>
                                    <li>Ir a Certificados: usuario actual &#8594; Personal &#8594; Certificados &#8594; Seleccionar el Certificado vigente (Emitido para: <b>SU nombre y apellido</b> / Emitido por:  <b>ad-Jusbaires-CA</b> / Fecha de expiración: <b>Constate que sea el certificado vencido a la fecha</b> ) &#8594; Boton derecho del mouse &#8594; <b>Eliminar</b></li>
                                    <img src={RC5} alt="fotoRC5"/>
                                    <li> Para confirmar la eliminación, presione <b>SI</b>.</li>
                                    <img src={RC6} alt="fotoRC6"/>
                                    <li>Para finalizar, cierre la ventana de la consola haciendo click en la X, Presione <b>NO</b> para no guardar la configuración.</li>
                                     <img src={RC7} alt="fotoRC6"/>

                                </ol>
                                <br/>
                                <h2>Instalación Certificado .pfx :</h2>
                                <ol>
                                    <li>Descargue en su máquina el certificado <b>(usuario.pfx)</b>.</li>
                                    <img src={RC8} alt="fotoRC7"/>
                                    <li>Instale el certificado, haciendo doble Click sobre el ícono generado en su escritorio.</li>
                                    <img src={RC9} alt="fotoRC9"/>
                                    <li>Luego, <b>Siguiente</b> en el asistente de importación de certificados:</li>
                                    <li>En el siguiente recuadro, seleccione <b>Usuario Actual</b>, luego <b>Siguiente</b>.</li>
                                    <img src={RC10} alt="fotoRC10"/>
                                    <li>Esta pantalla muestra la ruta donde se encuentra el certificado, presione <b>Siguiente</b>.</li>
                                    <img src={RC11} alt="fotoRC11"/>
                                    <li>En la siguiente pantalla, deberá colocar la <b>Contraseña para certificado</b> (usuario de dominio), debe dejar seleccionada la Opción: <b>“Incluir todas las propiedades extendidas”</b>, luego  <b>Siguiente</b>.</li>
                                    <img src={RC12} alt="fotoRC12"/>
                                    <li>Dejar seleccionada la opción <b>“Seleccionar automáticamente un almacén de certificados según el tipo de certificado”</b> luego, <b>Siguiente</b>.</li>
                                    <img src={RC13} alt="fotoRC13"/>
                                    <li>En la siguiente pantalla, Presione <b>Finalizar</b>.</li>
                                    <img src={RC14} alt="fotoRC14"/>
                                    <li>Finalmente <b>Aceptar</b> en la siguiente pantalla, para culminar el proceso de importación del certificado.</li>
                                    <img src={RC15} alt="fotoRC15"/>
                                </ol>
                            </div>
                       </div>
                    </div>
                 </div>
                 </div>
             </div>
             <div className="col-md-12"></div>
        
            
            
        </div>
        
        );
    }
}

export default RenCert;