import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';


class SectionB extends Component {

  render() {
      
    return (
         <div className=" greyBg">
            <div className="col-md-12"></div>
            <div className="col-md-12">
                <div id="sectionB" className="section">
                 <div className="container">
                    <div className="row">
                       <div className="col-md-12">
                            <div className="titlepage">
                                <h2>¿Por qué debo tener una VPN para conectarme al CMCABA?</h2>
                                <span>Para poder acceder a los servicios y aplicaciones del CMCABA es necesario conectarse a través de una Red Privada Virtual, permitiendo así realizar una conexión segura.</span>
                            </div>
                       </div>
                    </div>
                 </div>
                 </div>
             </div>
             <div className="col-md-12"></div>
         </div>
    )
  }
  
}

export default SectionB;