import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';
import image10 from '../images/MacSinCert/image10.png';
import image15 from '../images/MacSinCert/image15.png';
import image16 from '../images/MacSinCert/image16.png';
import image17 from '../images/MacSinCert/image17.png';
import image18 from '../images/MacSinCert/image18.png';



class MacEscritorioRemoto extends Component{
    
    render(){
        return(
            <div className="subsection">
             <div className="container">
                    <div className="row">
                       <div className="col-md-12">
                            <div className="titlepage">
                            <ol id={this.props.setClass}>
                                <li>
                                    <span><b>Descargar e Instalar el Acceso Remoto</b></span>
                                    <br/>
                                    <span><b><i>NOTA: Es importante tener actualizado el Sistema Operativo (OS X) del equipo a la última versión posible.</i></b></span>
                                    <ol>
                                        <li>Descargar el Acceso al Escritorio Remoto <a href="/React/public/downloads/TS-CMCABA.rdp" download>TS-CMCABA</a></li>
                                        <li>Descargar e instalar el <i>“Microsoft Remote Desktop"</i> directamente desde el AppStore de Apple <a href="https://apps.apple.com/es/app/microsoft-remote-desktop-10/id1295203466?mt=12">(click aquí)</a></li><img src={image10}/>
                                        <li>Al iniciar la descarga se abre el App Store que va a permitir dascargar e instalar la aplicación Microsoft Remote Desktop (esta aplicación es gratuita).</li>
                                        <li>Descargar. El App Store para proceder a la instalación va a solicitarle la contraseña del usuario administrador de la MAC. </li>
                                    </ol>
                                </li>
                                <br/>
                                <li>
                                    <span><b>Conectarse al Escritorio Remoto</b></span>
                                    <ol>
                                        <li>Haga click  en el archivo de Conexión al Escritorio Remoto, <b>TS-CMCABA.rdp</b> (previamente descargado).</li><img src={image15}/>
                                        <li>La conexión va a pedir “usuario” y “contraseña” (que utiliza para el acceso al CMCABA). Luego haga Click en <b>Continuar</b>.</li><img src={image16}/>
                                        <li>Se va a abrir una ventana indicando“…el certificado no se pudo verificar…”, hacemos click en <b>Continuar</b>.</li><img src={image17}/>
                                        <li>A continuación se va a abrir el Escritorio Remoto del CMCABA.</li><img src={image18}/>
                                    </ol>
                                    <span><b><i></i>NOTA: Este escritorio Remoto accede a las Carpetas a las que tiene acceso en los Discos G, H e I, no a su escritorio de Oficina.</b></span>
                                </li>
                            </ol>
                            </div>  
                        </div>  
                    </div>   
                </div>  
            </div>  
        );
    }
    
}

export default MacEscritorioRemoto;
