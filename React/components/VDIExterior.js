import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';
import image1 from '../images/LinuxConCert/image1.png';
import image2 from '../images/LinuxConCert/image2.png';
import image3 from '../images/LinuxConCert/image3.png';
import image4 from '../images/LinuxConCert/image4.png';
import image5 from '../images/LinuxConCert/image5.png';
import image6 from '../images/LinuxConCert/image6.png';
import image7 from '../images/LinuxConCert/image7.png';
import image8 from '../images/LinuxConCert/image8.png';
import image9 from '../images/LinuxConCert/image9.png';
import image10 from '../images/LinuxConCert/image10.png';
import image11 from '../images/LinuxConCert/image11.png';
import image12 from '../images/LinuxConCert/image12.png';
import image13 from '../images/LinuxConCert/image13.png';
import image14 from '../images/LinuxConCert/image14.png';
import image15 from '../images/LinuxConCert/image15.png';
import image16 from '../images/LinuxConCert/image16.png';
import image17 from '../images/LinuxConCert/image17.png';
import image18 from '../images/LinuxConCert/image18.png';
import image19 from '../images/LinuxConCert/image19.png';


class VDIExterior extends Component {
    render(){
    return(
        <div>
          <div className="col-md-12"></div>
            <div className="col-md-12">
                <div id="subsectionLinuxConCert" className="subsection">
                 <div className="container">
                    <div className="row">
                       <div className="col-md-12">
                            <div className="titlepage">
                                <h2>Acceso Remoto VDI desde fuera de Argentina</h2>
                                <br/>
                                <ol start="1">
                                    <li>
                                        <span><b>Permisos:</b></span>
                                        <ol>
                                            <li>El usuario debe comunicarse con mesa de ayuda a través del e-mail <div className="styled-link">meayuda@jusbaires.gob.ar</div> o al teléfono +54911-40080300 para solicitar el permiso correspondiente para conectarse a VDI desde el exterior del país.</li>
                                            <li>Mesa de ayuda asigna al usuario el permiso de <b>VPN-VDI</b>.</li>
                                            <li>Se debe esperar a que replique el permiso, esto ocurre cada media hora aproximadamente.</li>
                                        </ol>
                                    </li>
                                    <br/>
                                    <li>
                                        <span><b>Descarga el cliente de VPN Cisco Anyconnect</b></span>
                                        <ol>
                                            <li> Descargar el Cisco Anyconnect, desde los siguientes link: (elija el que corresponde a su sistema operativo)
                                                <ol>
                                                    <li>Para MAC: <a href="https://drive.juscaba.gob.ar/s/ToGPXNBDaBposbT" target="_blank"></a></li>
                                                    <li>Para Windows: <a href="https://drive.juscaba.gob.ar/s/bypWgD2JYDaB7Ke" target="_blank"></a></li>
                                                </ol>
                                            </li>
                                            <li>Una vez descargado el archivo presione click derecho para extraer todo y elija en su PC donde desea guardar los archivos de instalación.</li>
                                            <li>Al finalizar la descarga abrir la carpeta descargada (Ejemplo de cómo se visualiza en Windows):</li><img src={image1}/>
                                        </ol>
                                    </li>
                                    <br/>
                                    <li>
                                        <span><b>Instalación del cliente de VPN Cisco Anyconnect</b></span>
                                        <ol>
                                            <li>Presione doble click en <b>“Setup.exe”</b>. Visualizará una ventana como la siguiente:</li><img src={image2}/>
                                            <li>Seleccione solo la primera opción <b>“Core & AnyConnect VPN”</b> y luego presione Install <b>Selected</b>. Siga los pasos por defecto hasta finalizar.</li>
                                        </ol>
                                    </li>
                                    <br/>
                                    <li>
                                        <span><b>Conexión de VPN a través de Cisco Anyconnect</b></span>
                                        <span><i>Este paso lo debe hacer cada vez que requiera conectarse al VDI</i></span>
                                        <ol>
                                            <li>Ubique la APP <b>"Cisco Anyconnect”</b>, se va a ejecutar una ventana como la siguiente, y en el espacio en blanco debe escribir: <b>vp.jusbaires.gob.ar</b></li><img src={image3}/>
                                            <li>Presione el botón <b>Connect</b>, abre otra ventana como se muestra a continuación:</li><img src={image4}/>
                                            <li>Allí debe configurar de la siguiente manera:
                                                <ol>
                                                    <li><b>Grupos:</b> VPN-VDI</li>
                                                    <li><b>Username:</b> Usuario CMCABA sin @jusbaires (igual al correo jusbaires) por ejemplo: frfernandez </li>
                                                    <li><b>Password:</b> Clave de Usuario CMCABA (igual al correo jusbaires)</li>
                                                </ol>
                                                Ejemplo:
                                                <img src={image5}/> 
                                            </li>
                                            <li>Presione <b>OK</b> y la VPN se conectará  automáticamente.</li>
                                        </ol>
                                    </li>
                                    <br/>
                                    <li>
                                        <span><b>Configuración a VDI (a través de la aplicación de escritorio para poder firmar digitalmente con token)</b></span>
                                        <span><i>Si usted requiere firmar digitalmente en alguno de los sistemas Eje, SISTEA o Firmador Digital, entonces deberá acceder a través de la aplicación de escritorio que se detalla a continuación</i></span>
                                        <ol>
                                            <li>Ingrese al siguiente enlace  <a href="vdi.jusbiares.gob.ar" target="_blank"></a> desde su navegador.</li>
                                            <li>Luego descargue la aplicación de escritorio para acceder a VDI. Presione la Opción: <b>“Descargar Horizon Client para Windows (x64)”</b></li><img src={image6}/>
                                            <li>Seleccione <b>“Omnissa Horizont Clients”</b></li><img src={image7}/>
                                            <li>Seleccione el cliente <b>Omnissa Horizont Client</b>, que corresponda a su sistema operativo, presionando <b>GO TO DOWNLOADS</b>.</li><img src={image8}/>
                                            <li>Presione <b>DOWNLOAD NOW</b>, por ejemplo para Windows:</li><img src={image9}/>
                                            <li>Se iniciará la descarga del cliente Ominissa:</li><img src={image10}/>
                                            <li>Ubique la carpeta de descargas en su equipo, y presione doble click sobre <b>“Omnisa-Horizon-Client….”</b></li><img src={image11}/>
                                            <li>Una vez descargado el programa presione <b>“Aceptar e instalar”</b> y espere unos minutos hasta que se instale y presione <b>“Finalizar”</b></li><img src={image12}/>
                                            <li>Presione <b>“Reiniciar ahora”</b> (se reiniciará su PC)</li><img src={image13}/>
                                            <li>Ubique y abra la aplicación <b>“Omnissa Horizont Client”</b> en el escritorio de su PC, luego presione <b>“Agregar Servidor”</b></li><img src={image14}/>
                                            <li>En la siguiente pantalla, en <b>“Nombre de Connection Server”</b>, escriba: <b>vdi.jusbaires.gob.ar</b> y presione el botón <b>Conectar</b>.</li><img src={image15}/>
                                            <li>Introduzca su nombre de usuario y contraseña (igual al correo electrónico, pero sin @jusbaires.gob.ar) y presione <b>"Iniciar sesión"</b>.</li><img src={image16}/>
                                            <li>Seleccione con doble click el ícono de <b>VDI</b>, a partir de este momento podrá hacer uso de las aplicaciones del CMCABA.</li><img src={image17}/>
                                        </ol>
                                    </li>
                                    <br/>
                                    <li>
                                        <span><b>Conexión a VDI (luego de la 1era configuración)</b></span>
                                        <ol>
                                            <li>Ubique el ícono <b>Omnissa Horizont Client</b> y luego presione el botón <b>vdi.jusbaires.gob.ar</b>, que ya se encuentra configurado</li><img src={image18}/>
                                            <li>Introduzca su nombre de usuario y contraseña (igual al correo electrónico, pero sin @jusbaires.gob.ar)</li><img src={image19}/>
                                        </ol>
                                    </li>
                                </ol>
                            </div>
                       </div>
                    </div>
                 </div>
                 </div>
             </div>
             <div className="col-md-12"></div>
        </div>
        );
    }
}

export default VDIExterior;
