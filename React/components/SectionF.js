import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';
import Collapsible2 from './Collapsible2.js';


class SectionF extends Component {

  render() {
      
    return (
         <div className="greyBg">
            <div className="col-md-12"></div>
            <div className="col-md-12">
                <div id="sectionF" className="section">
                 <div className="container">
                    <div className="row">
                       <div className="col-md-12">
                            <div className="titlepage">
                                <h2>Tengo un nuevo certificado.pfx renovado, ¿qué debo hacer para reinstalarlo?</h2>
                                <br/>
                                <span>A continuación se presentan los pasos a seguir para la <b>reinstalación de un certificado de VPN,</b> aplica en los casos de Renovación de Certificados.</span>
                                <br/>
                                <br/>
                                <Collapsible2/>
                                <br/>
                            
                            </div>
                       </div>
                    </div>
                 </div>
                 </div>
             </div>
             <div className="col-md-12"></div>
         </div>
    )
  }
  
}

export default SectionF;