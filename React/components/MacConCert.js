import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';
import image1 from '../images/MacConCert/image1.png';
import image2 from '../images/MacConCert/image2.png';
import image3 from '../images/MacConCert/image3.png';
import image4 from '../images/MacConCert/image4.png';
import image5 from '../images/MacConCert/image5.png';
import image6 from '../images/MacConCert/image6.png';
import image7 from '../images/MacConCert/image7.png';
import image8 from '../images/MacConCert/image8.png';
import image9 from '../images/MacConCert/image9.png';
import image10 from '../images/MacConCert/image10.png';
import image11 from '../images/MacConCert/image11.png';
import image12 from '../images/MacConCert/image12.png';
import image13 from '../images/MacConCert/image13.png';
import image14 from '../images/MacConCert/image14.png';
import image15 from '../images/MacConCert/image15.png';
import image16 from '../images/MacConCert/image16.png';
import image17 from '../images/MacConCert/image17.png';
import image18 from '../images/MacConCert/image18.png';
import image19 from '../images/MacConCert/image19.png';
import image20 from '../images/MacConCert/image20.png';
import image21 from '../images/MacConCert/image21.png';
import image22 from '../images/MacConCert/image22.png';
import image12SinCert from '../images/MacSinCert/image12.png';




class MacConCert extends Component {
  render() {
    return (
        <div>
          <div className="col-md-12"></div>
            <div className="col-md-12">
                <div id="subsectionMacConCert" className="subsection">
                 <div className="container">
                    <div className="row">
                       <div className="col-md-12">
                            <div className="titlepage">
                                <h2>Configuración de su computadora personal para acceso remoto con Sistema Operativo MacOS - Con Certificado</h2>
                                <span><b>Este procedimiento se realiza solo la primera vez que va a instalar la VPN </b></span>
                                <br/>
                                <ol start="1">
                                    <li>
                                        <span><b>Descargar el certificado <i>(usuario.pfx)</i> Enviado por mail.</b></span>
                                    <ol>
                                        <li>Descargar en su máquina el certificado <b><i>(usuario.pfx)</i></b>. Una vez descargado, debe hacer doble click en el icono.</li><img src={image1}/>
                                        <li>Se desplegará, la siguiente ventana. Verificar que en el campo <b>llavero</b> se mantenga el valor <b>inicio de sesión</b>, tal como se señala en la imagen. Luego, <b>Agregar</b>.</li><img src ={image2}/>
                                        <li>En la siguiente ventana debe completar el campo <b>contraseña</b>. En este caso es el usuario de red. Luego hacer click sobre el botón <b>OK</b>.</li><img src={image3}/>
                                        <li>Una vez completado el paso anterior, se muestra en la carpeta de llaveros el certificado, tal como se muestra en la imagen. Hacer doble click sobre el certificado.</li><img src={image4}/>
                                        <li>En la siguiente pantalla debe hacer click sobre la opción <b>Confiar</b>.</li><img src={image5}/>
                                        <li>Después de seleccionar la opción confiar, se muestra la siguiente pantalla.</li><img src={image6}/>
                                        <li>Hacer click sobre <b>Al utilizar este certificado</b> y seleccionar de la lista <b>confiar siempre</b>.</li><img src={image7}/>
                                        <li>Luego se muestra la siguiente ventana un dialogo para confirmar los cambios, en el campo <b>contraseña</b>, escribir la que corresponde al usuario administrador del equipo.
                                        Hacer click sobre <b>Actualizar configuración</b>.</li><img src={image8}/>
                                        <li> Posteriormente es posible verificar que el certificado ha sido instalado con éxito.</li><img src={image9}/>
                                    </ol>
                                    </li>
                                <br/>
                                    <li>
                                        <span><b>Instalacion Software Cisco AnyConnect</b></span>
                                    <ol>
                                        <li>Descargar la aplicación cliente <a href="/React/public/downloads/MacOS/anyconnect-macos-4.9.02028-predeploy-k9.dmg" download>Cisco AnyConnect MacOS (click aquí)</a></li> Al finalizar descarga, hacer doble click sobre el icono respectivo.<img src={image10}/>
                                        <li>Se muestra la siguiente pantalla. Hacer doble click sobre el icono señalado</li><img src={image11}/>
                                        <li>Se despliega el asistente de instalación. Hacer en <b>Continuar</b>.</li><img src={image12}/>
                                        <li>En la siguiente pantalla se muestra el contrato de licencia, hacer click sobre el botón <b>Continuar</b>.</li><img src={image13}/>
                                        <li>Hacer click sobre el botón <b>Acepto</b>.</li><img src={image14}/>
                                        <li>En la siguiente imagen los valores predeterminados, deben permanecer tal como se muestran. Hacer click sobre el botón <b>Continuar</b>.</li><img src={image15}/>
                                        <li>En la siguiente pantalla, hacer click sobre el botón <b>Instalar</b>.</li><img src={image16}/>
                                        <li>En el siguiente cuadro de diálogo, ingresar la contraseña de administrador del equipo. Luego hacer click sobre el botón <b>Instalar Software</b>.</li><img src={image17}/>
                                        <li>Se muestra a continuación la imagen que confirma que ha finalizado el proceso de instalación de manera exitosa.</li><img src={image18}/>
                                    </ol>
                                    </li>
                                <br/>
                                    <li>
                                        <span><b>Conexión a la VPN</b></span>
                                    <br/>

                                    <span><b>Este procedimiento lo va a realizar cada vez que desee conectarse a la VPN</b></span>
                                    <ol>
                                        <li>Ejecutar el <b><i>“Cisco AnyConnect Secure Mobility Client”</i></b>. El aplicativo se encuentra en la carpeta de aplicaciones, tal como se muestra en la siguiente imagen.</li><img src={image19}/>
                                        <li>Se despliega en la siguiente pantalla la interfaz de acceso al servicio.</li><img src={image20}/>
                                        <li>En el campo <b>Ready to connect escribir</b>: <i>vp.jusbaires.gob.ar</i>. Luego hacer click sobre el botón <b>Connect</b>.</li><img src={image12SinCert}/>
                                        <li>En la siguiente pantalla completar los campos correspondientes, tal como se indica a continuación:
                                            <ol>
                                                <li>En <b><i>Group:</i></b> seleccionar de la lista: <b><i>ConCertificado</i></b>.</li>
                                                <li><b><i>Username:</i></b> el asignado por el CMCABA</li>
                                                <li><b><i>Password:</i></b> contraseña de red asignada por el CMCABA.</li>
                                                <li>Finalmente presionar <b>OK.</b></li>
                                            </ol>
                                        </li><img src={image22}/>
                                    </ol>
                                    </li>
                                    <span><b><i></i>NOTA: El certificado tendrá vigencia de 1 (un) año y la contraseña de red de 6 (seis) meses contados a partir de la fecha de asignación de los mismos. Para mayor
                                    información comunicarse con la mesa de Ayuda a través de <a href="mailto:meayuda@jusbaires.gob.ar">meayuda@jusbaires.gob.ar</a> o Tel: 4008-0300.</b></span>
                                </ol>
                            </div>
                       </div>
                    </div>
                 </div>
                 </div>
             </div>
             <div className="col-md-12"></div>
        </div>
        )
    }
}

export default MacConCert;
