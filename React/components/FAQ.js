import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';
import WinEscritorioRemoto from './WinEscritorioRemoto.js';
import connect from '../images/WindowsSinCert/connect.png';

class FAQ extends Component {
    render() {
    return (
         <div className="whiteBg">
            <div className="col-md-12"></div>
            <div className="col-md-12">
                <div id="sectionB" className="section">
                 <div className="container">
                    <div className="row">
                       <div className="col-md-12">
                            <div className="titlepage faq">
                                <h2>Preguntas Frecuentes</h2>
                                <ol>
                                    <li><b><i>Ya tengo todo configurado, ¿cómo me conecto a la red CMCABA?</i></b></li>
                                        <div>
                                            <div className="subsection">
                                                <div className="container">
                                                    <div className="row">
                                                       <div className="col-md-12">
                                                            <div className="titlepage">
                                                                <span><b>Conectarse al Escritorio Remoto</b></span>
                                                                <ol>
                                                                    <li>Ejecute la aplicación Cisco AnyConnect. Presione <b>Conectar</b>. Verifique que tenga un candado con un tilde verde y la leyenda “Connected to vp.jusbaires.gob.ar”.</li>
                                                                    <img src={connect} alt="consejo"></img>
                                                                    <li>Busque el ícono TS-CMCABA previamente copiado en el escritorio.</li>
                                                                    <li>Doble click en el ícono TS-CMCABA y presione <b>Conectar</b>.</li>
                                                                    <li>Escriba credenciales (Usuario y Contraseña otorgados por el CMCABA), seleccione Recordar mis Credenciales y Aceptar</li>
                                                                    <br/>
                                                                    <span><b>En este momento tendrá acceso al escritorio Remoto.</b></span>
                                                                </ol>
                                                            </div>  
                                                        </div>  
                                                    </div>   
                                                </div>  
                                            </div>  
                                        </div>
                                    <br/>
                                    <br/>
                                    <li><b><i>Se me venció el certificado de acceso o tengo problemas con mi certificado digital
                                    de acceso remoto</i></b></li>
                                    <br/>
                                    Si al intentar conectarse a la VPN le aparece un cartel indicando que ha vencido su certificado, 
                                    deberá comunicarse con la Mesa de Ayuda a fin de solicitar su renovación, para ello debe suministrar los siguientes datos: 
                                    <b>Nombre y Apellido</b>, <b>Usuario asignado a la VPN</b> y <b>Mensaje de Error</b>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <li><b><i>¿Puedo conectarme de forma remota desde más de una PC?</i></b></li>
                                    <br/>
                                    Si, es posible conectarse desde más de una PC, para ello deberá realizar esta misma instalación en cada PC desde la cual necesite conectarse.
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <li><b><i>No puedo escuchar el audio dentro de mi conexión remota al TS</i></b></li>
                                    <br/>
                                    Dentro de la ventana del TS no podrá reproducir sonidos ni audios. Esto se debe a la optimización del sistema de conexión remota lo cual brinda
                                    una performance de velocidad y estabilidad mucho más alta frente a la habilitación del sonido. Por lo que no podrá reproducir audios de whatsapp,
                                    ni tampoco usar el sistema de Videoconferencia Cisco Webex Meetings. Para utilizar los mismos, deberá hacerlo directamente desde la sesión normal 
                                    de su computadora <b>(no dentro de la ventana de TS)</b>.
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <li><b><i>Estoy accediendo a una videoconferencia y no puedo escuchar ni me funciona el micrófono</i></b></li>
                                    <br/>
                                    Dentro de la ventana del TS no podrá reproducir sonidos ni audios. Esto se debe a la optimización del sistema de conexión remota lo cual brinda
                                    una performance de velocidad y estabilidad mucho más alta frente a la habilitación del sonido. Por lo que no podrá reproducir audios de whatsapp,
                                    ni tampoco usar el sistema de Videoconferencia Cisco Webex Meetings. Para utilizar los mismos, deberá hacerlo directamente desde la sesión normal 
                                    de su computadora <b>(no dentro de la ventana de TS)</b>.
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <li><b><i>¿Cómo se si estoy conectado a la red CMCABA?</i></b></li>
                                    <br/>
                                    Haciendo click en la aplicación Cisco AnyConnect, esta tendrá un candado con un tilde verde y la leyenda “Connected to acceso.jusbaires.gob.ar”
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <li><b><i>¿Es posible conectarse de forma remota a la PC donde trabajo en la oficina?</i></b></li>
                                    <br/>
                                    No es posible conectarse a su PC, pero va a tener disponibles todas las unidades de red exactamente igual que en la PC de su lugar de trabajo.
                                    <br/>
                                    Si tiene la seguridad que necesita de documentos que están guardados en el escritorio de su PC en la oficina, y/o en alguna carpeta local como ser 
                                    ‘Mis Documentos’, ‘Descargas’, o cualquier carpeta creada localmente en la unidad disco C:\ , deberá copiar y/o mover presencialmente desde la PC de su oficina, 
                                    toda documentación que considere necesaria y útil para desarrollar normalmente el trabajo de forma remota, a la unidad H:\ (unidad personal), a la unidad I:\ 
                                    (Grupos) y/o a la unidad P:\ (Público)
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <li><b><i>¿Hice todos los pasos del instructivo pero no puedo conectarme, puedo solicitar asistencia remota para que un técnico revise mi equipo?</i></b></li>
                                    <br/>
                                    Si, comuníquese con la Mesa de Ayuda por mail a <a href="mailto:meayuda@jusbaires.gob.ar">meayuda@jusbaires.gob.ar</a> o al Teléfono: 4008-0300. Mientras tanto y para ganar tiempo, descargue e 
                                    instale la siguiente aplicación de asistencia remota <a href="/React/public/downloads/MacOS/AnyDesk.dmg" download>AnyDesk MacOS (click aquí)</a> o <a href="/React/public/downloads/Windows/AnyDesk.exe" download>AnyDesk Windows (click aquí)</a> .
                                    <br/>
                                    <br/>
                                    <li><b><i>Tengo un nuevo certificado.pfx renovado, ¿qué debo hacer para reinstalarlo?</i></b></li>
                                    <br/>
                                    Para reinstalar el certificado siga las instrucciones indicadas <b><i><a href='#saltoSectionF'>aquí.</a></i></b>
                                    <br/>
                                   
                                </ol>
                            </div>
                       </div>
                    </div>
                 </div>
                 </div>
             </div>
             <div className="col-md-12"></div>
         </div>
    )
  }
  
}



export default FAQ;

    