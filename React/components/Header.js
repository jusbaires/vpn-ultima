import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';
import img from '../images/logo_cmcaba.png';
import logout from '../images/Logout.png';


class Header extends Component {
  
  logOut(){
    return;
  }
  
  render() {
    return (
      <div>
        <header>
           <div className="header">
               <div className="container-fluid">
                  <div className="row">
                    <div className="col-md-11">
                      <img src={img} alt="logo"></img>
                    </div>
                    {/*<div className="col-md-1">
                      <a onClick={this.logOut}><img src={logout} className= "logout"></img></a>
                    </div>*/}
                  </div>
               </div>
           </div>
        </header>
      </div>

    )
  }
}

export default Header;