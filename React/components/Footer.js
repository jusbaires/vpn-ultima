import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';



class Footer extends Component {
  render() {
      
    return (
        
            <div id="cmc-footer" >
                <div id="fondo">
                </div>
                
                   <div id="cmc-footer-wrapper">
                   
                       <div id="cmc-footer-datos">
                       
                           <div id="cmc-footer-contacto">
                               <ul>
                                   <li id="cmc-footer-direccion">Av. Julio A. Roca 516 - C.A.B.A. (C1067ABN)</li>
                                   <li id="cmc-footer-telefono">(+5411) 4008-0200</li>
                               </ul>
                           </div>
                           
                           <div id="cmc-footer-social">
                               <ul>
                                   <li id="cmc-footer-facebook">
                                       <a className="linkIconSocial " href="https://www.facebook.com/consejomcaba"></a>
                                       <a className="linkLabelSocial " href="https://www.facebook.com/consejomcaba">/consejomcaba</a>
                                   </li>
                                   <li id="cmc-footer-twitter">
                                       <a className="linkIconSocial " href="https://www.twitter.com/consejomcaba"></a>
                                       <a className="linkLabelSocial " href="https://www.twitter.com/consejomcaba">@consejomcaba</a>
                                   </li>
                                   <li id="cmc-footer-youtube">
                                       <a className="linkIconSocial " href="https://www.youtube.com/ConsMagistraturaCABA"></a>
                                       <a className="linkLabelSocial" href="https://www.youtube.com/user/ConsMagistraturaCABA">ConsMagistraturaCABA</a>
                                   </li>
                               </ul>
                           </div>
                       </div>
                   </div>
            </div>
        
    )
  }
  
  
}


export default Footer;