import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';
import imgWin from '../images/WindowsSinCert/EjecutarWindows.png';
import imgNext1 from '../images/WindowsSinCert/next1.png';
import imgNext2 from '../images/WindowsSinCert/next2.png';
import imgInstall from '../images/WindowsSinCert/install.png';
import imgFinish from '../images/WindowsSinCert/finish.png';
import connect from '../images/WindowsSinCert/connect.png';
import AnyConnect from '../images/WindowsSinCert/AnyConnect.png';
import ConCert from '../images/WindowsSinCert/ConCert.png';
import usuarioPFX from '../images/usuarioPFX.png';
import actualSiguiente from '../images/actualSiguiente.png';
import luegoSiguiente from '../images/luegoSiguiente.png';
import contraSiguiente from '../images/contraSiguiente.png';
import selectAlmacen from '../images/selectAlmacen.png';
import finalizar from '../images/finalizar.png';
import guardarTS from '../images/guardarTS.png';
import escritorioTS from '../images/escritorioTS.png';
import capturaEscritorio from '../images/capturaEscritorio.png';
import WinEscritorioRemoto from './WinEscritorioRemoto.js';

class WindowsConCert extends Component {
    render(){
    return(
        <div>
          <div className="col-md-12"></div>
            <div className="col-md-12">
                <div id="subsectionMacSinCert" className="subsection">
                 <div className="container">
                    <div className="row">
                       <div className="col-md-12">
                            <div className="titlepage">
                                <h2>Configuración de su computadora personal para acceso remoto con Sistema Operativo Windows - Con Certificado</h2>
                                <span>A continuación se describen los pasos necesarios a fin de realizar la descarga e instalación de aplicativos para la conexión remota al CMCABA.</span>
                                <br/>
                                <ol start="1">
                                    <li>
                                        <span><b>Descargar e Instalar el certificado de VPN(.pfx).</b> Este procedimiento lo realizará <p style={{color:"red"}}>sólo una vez.</p></span>

                                        <ol>
                                            <li>Descargue en su computadora el certificado .pfx enviado por mail</li><img className="fotoPFX" src={usuarioPFX} alt="capturaCertificado"/>
                                            <li>Presione doble click sobre certificado .pfx</li>
                                            <li>En la pantalla del asistente para importar, seleccione <b>Usuario actual</b> y presione <b>Siguiente:</b></li>
                                            <img src={actualSiguiente} alt="capturaActSig"/>
                                            <br/>
                                            <li>Luego, <b>Siguiente:</b></li>
                                            <img src={luegoSiguiente} alt="luegoSiguiente"/>
                                            <br/>
                                            <li>Introduzca la contraseña asignada (enviada por mail junto al certificado .pfx) y luego presione <b>Siguiente:</b></li>
                                            <img src={contraSiguiente} alt="contraSiguiente"/>
                                            <br/>
                                            <li>Seleccione la opción <b>"Seleccionar automáticamente el almacén de certificados según el tipo de certificado” </b>y presione <b>Siguiente:</b></li>
                                            <img src={selectAlmacen} alt="selectAlmacen"/>
                                            <br/>
                                            <li>Para concluir con la instalación del certificado .pfx presione <b>Finalizar:</b></li>
                                            <img src={finalizar} alt="finalizar"/>
                                            <br/>
                                        </ol>
                                    </li>
                                <br/>
                                    <li>
                                        <span><b>Descargar e Instalar Software Cisco AnyConnect</b></span>

                                    <p>Este procedimiento lo realizará <b>sólo una vez</b>, para la descarga e instalación del aplicativo <a href="/React/public/downloads/Windows/anyconnect-win-4.8.03052-core-vpn-predeploy-k9.msi">Cisco AnyConnect (click aquí)</a></p>
                                        <ol>
                                            <li>Ejecutar archivo</li><img src={imgWin} alt="capturaEjecucion"/>
                                            <li>Next en la siguiente pantalla:</li><img src={imgNext1} alt="capturaNext"/>
                                            <li>A continuación aparecerá  la licencia de usuario, la cual debe aceptar “I accept the terms in the Licence Agreement” y presionar Next:</li>
                                            <img src={imgNext2} alt="capturaNext"/>
                                            <br/>
                                            <li>A continuación, presione  Install:</li>
                                            <img src={imgInstall} alt="capturaInstall"/>
                                            <br/>
                                            <li>Una vez finalizada la instalación, presione Finish en la siguiente pantalla (tardará unos minutos):</li>
                                            <img src={imgFinish} alt="capturaFinish"/>
                                            <br/>
                                        </ol>
                                    </li>
                                <br/>
                                <li>
                                    <span><b>Pasos a seguir para realizar la Conexión a la VPN del CMCABA</b></span>

                                    <ol>
                                        <li>Primero debe conectarse a la VPN</li>
                                        <br/>
                                        <p>Este procedimiento deberá ejectuarlo <u>cada vez que requiera conectarse a la VPN</u> y al escritorio Remoto (TS-CMCABA). Para ello AMBOS ÍCONOS DEBEN ESTAR EN EL ESCRITORIO DE LA PC.</p>
                                        <br/>
                                        <img src={capturaEscritorio} alt="capturaEscritorio"/>
                                        <li>Ejecute el icono <i>AnyConnect</i> que se encuentra en la parte inferior izquierda de la pantalla</li>
                                        <img src={AnyConnect} alt="capturaAnyConnect"/>
                                        <br/>
                                        <li>En caso de que el recuadro aparezca en blanco, escriba: vp.jusbaires.gob.ar, luego presione Connect</li>
                                        <img src={connect} alt="connect"/>
                                        <br/>
                                        <li>En el siguiente paso será solicitado el grupo y el password de acceso de red. Dónde:</li>
                                            <ol type="a" className="tabbed">
                                                <li>En Group: Debe seleccionar de la lista: <b>ConCertificado.</b></li>
                                                <li>Password: Introduzca la Contraseña de red asignada por el CMCABA.</li>
                                                <li>Finalmente presione OK.</li>
                                                <img src={ConCert} alt="ConCert"/>
                                                <br/>
                                                <div className="alert alert-danger" role="alert">
                                                <span><b>NOTA:</b> El certificado de VPN tendrá vigencia de 1 (un) año y la contraseña de red de 6 (seis) meses contados a partir de la fecha de asignación de los mismos. Para mayor información comunicarse con la mesa de ayuda a través de: <a href="mailto:meayuda@jusbaires.gob.ar">meayuda@jusbaires.gob.ar</a></span>
                                                </div>
                                            </ol>
                                    </ol>
                                </li>
                                </ol>
                            </div>
                            <WinEscritorioRemoto setClass={'startFive'} display={'noDisplay'}/>
                       </div>
                    </div>
                 </div>
                 </div>
             </div>
             <div className="col-md-12"></div>
        
            
            
        </div>
        
        );
    }
}

export default WindowsConCert;
