import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';


class SectionD extends Component {

  render() {
      
    return (
         <div className=" greyBg">
            <div className="col-md-12"></div>
            <div className="col-md-12">
                <div id="sectionD" className="section">
                 <div className="container">
                    <div className="row">
                       <div className="col-md-12">
                            <div className="titlepage">
                                <h2>¿Cómo obtener mi acceso remoto a la red del CMCABA?</h2>
                                <span>Deberá comunicarse a la Mesa de Ayuda a través de los siguientes canales:</span>
                                <br/>
                                <span>Mail: <a href="mailto:meayuda@jusbaires.gob.ar">meayuda@jusbaires.gob.ar</a> o  Teléfono: 4008-0300</span>
                                <br/>
                                <br/>
                                <span>Suministrar los siguientes datos en su solicitud:</span> 
                                <br/>
                                <span> <b>Título:</b> Solicitud de acceso remoto (VPN)<br/><b>Nombre y Apellido:</b><br/><b>Dependencia:</b><br/><b>Mail:</b><br/><b>Número Telefónico:</b><br/></span>

                            </div>
                       </div>
                    </div>
                 </div>
                 </div>
             </div>
             <div className="col-md-12"></div>
         </div>
    )
  }
  
}

export default SectionD;