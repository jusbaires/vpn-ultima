import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';


class SectionC extends Component {

  render() {
      
    return (
         <div className="whiteBg">
            <div className="col-md-12"></div>
            <div className="col-md-12">
                <div id="sectionC" className="section section-padbot">
                 <div className="container">
                    <div className="row">
                       <div className="col-md-12">
                            <div className="titlepage">
                                <h2>¿Qué es un TS?</h2>
                                <span>Los servicios de escritorio remoto o TS <b>(Terminal Service)</b> o Servicio de Terminal, son un componente que permite a los usuarios acceder a las aplicaciones y datos almacenados en los servidores del CMCABA mediante un acceso por red. <br/> A lo largo del instructivo leerá muchas veces el término TS, al referirnos a él estaremos haciendo mención práctica al ícono que descargará mas adelante, que al ejecutarlo con doble click  el mismo abre una ventana de conexión remota a la red del CMCABA.</span>
                            </div>
                       </div>
                    </div>
                 </div>
                 </div>
             </div>
             <div className="col-md-12"></div>
         </div>
    )
  }
  
}

export default SectionC;

