import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Styles.css';
import image1 from '../images/LinuxConCert/image1.png';
import image2 from '../images/LinuxConCert/image2.png';
import image3 from '../images/LinuxConCert/image3.png';
import image4 from '../images/LinuxConCert/image4.png';
import image5 from '../images/LinuxConCert/image5.png';

class LinuxConCert extends Component {
    render(){
    return(
        <div>
          <div className="col-md-12"></div>
            <div className="col-md-12">
                <div id="subsectionLinuxConCert" className="subsection">
                 <div className="container">
                    <div className="row">
                       <div className="col-md-12">
                            <div className="titlepage">
                                <h2>Configuración de su computadora personal para acceso remoto con Ubuntu - Con Certificado</h2>
                                <br/>
                                <ol start="1">
                                    <li>
                                        <span><b>Instalar Openconnect</b></span>
                                        <ol>
                                            <li>Descargar el certificado .pfx siguiendo las instrucciones enviadas en el mail que contiene el mismo.</li>
                                            <li>Descargar los paquetes necesarios para utilizar el servicio:</li>
                                            <span className="listtab">
                                                <b><i>sudo apt-get -y install openconnect network-manager-openconnect network-manager-openconnect-gnome openssl</i></b></span> 
                                            <li>Una vez instalado el Openconnect, dirijase a la carpeta donde descargó el certificado .pfx</li>
                                            <li>Haga click derecho / Renombrar Archivo, y cambie la extensión del archivo a .p12</li><img src={image1}/>
                                            <li>Presione “Cambiar nombre” y cierre la carpeta.</li>
                                        </ol>
                                    </li>
                                    <br/>
                                    <li>
                                        <span><b>Configurar la VPN</b></span>
                                        <ol>
                                            <li>Diríjase a la configuración de red y despliegue las opciones de red:</li><img src={image2}/>
                                            <li>Configuración de red cableada:
                                                <ol>
                                                    <li>Presione el ícono cableado
                                                        <ol>
                                                            <li>Seleccionar VPN y tilde el +. Elija Cliente VPN (openconnect)</li>
                                                            <li>En la siguiente pantalla configure de la siguiente manera:
                                                                <ol>
                                                                    <li>Protocolo VPN:  Cisco AnyConnect o OpenConnect:
                                                                        <ol>
                                                                            <li>Pasarela:  vp.jusbaires.gob.ar</li>
                                                                            <li>Certificado USR: Seleccione el archivo que renombró anteriormente.</li>
                                                                        </ol>
                                                                    </li>
                                                                </ol>
                                                            </li>
                                                        </ol>
                                                    </li>
                                                </ol>
                                            </li>
                                            <li>Resultado de esta configuración:</li><img src={image3}/>
                                            <li>Luego presione el botón AÑADIR.  Va a observar la configuración de VPN 1 creada.
                                                <ol>
                                                    <li>Actívela y va a visualizar la siguiente pantalla:</li><img src={image4}/>
                                                    <li>Introduzca la CLAVE DEL CERTIFICADO (esta clave es la misma que el usr, por ejemplo: certificado: “frfernandez” clave “frfernandez”), enviada en el mail junto al certificado.</li>
                                                    <li>En la siguiente pantalla, complete con las credenciales de USUARIO DE DOMINIO (usuario del certificado y contraseña asignada en el sitio miclave)</li><img src={image5}/>
                                                </ol>
                                            </li>   
                                        </ol>
                                    </li>
                                </ol>
                            </div>
                       </div>
                    </div>
                 </div>
                 </div>
             </div>
             <div className="col-md-12"></div>
        </div>
        );
    }
}

export default LinuxConCert;
